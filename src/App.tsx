import React from 'react';
import './App.css';
import Nav from "./components/Nav";
import {BrowserRouter, Route} from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login"
import Register from "./pages/Register";
import Menu from "./pages/Menu";
import ProvideParts from "./pages/ProvideParts";
import TransactionSuccess from "./pages/TransactionSuccess";
import { Container } from 'react-bootstrap';
import GetAllTransfersByASupplier from "./pages/GetAllTransfersByASupplier";
function App() {
	return (
		<Container>
		<div className="App">
			<BrowserRouter>
				<Nav />
				<main className="form-login">
					<Route path="/" exact component={Home}/>
					<Route path="/login" component={Login}/>
					<Route path="/register" component={Register}/>
					<Route path="/menu" component={Menu}/>
					<Route path="/provide_parts" component={ProvideParts}/>
					<Route path="/get_all_transfers_by_a_supplier" component={GetAllTransfersByASupplier}/>
					<Route path="/transaction_success" component={TransactionSuccess}/>
				</main>
			</BrowserRouter>
		</div>
		</Container>
	);
}

export default App;
