import React, { useState, useEffect } from 'react'
import axios from 'axios'

const URL = '/supplier/provide_parts'

const GetAllTransfersByASupplier = () => {
    const [employees, setEmployees] = useState([])

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {

        const response = await axios.get(URL, {headers: {Authorization: "supplier2"}})
            setEmployees(response.data.data)

    }

    const renderHeader = () => {
        let headerElement = ['batch_id', 'sku_number','sku_name','supplier_name', 'manufacture_date', 'oem']

        return headerElement.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    const renderBody = () => {
        return employees && employees.map(({ batch_id, sku_number, sku_name, supplier_name, manufacture_date, oem }) => {
            return (
                <tr key={batch_id}>
                    <td>{batch_id}</td>
                    <td>{sku_number}</td>
                    <td>{sku_name}</td>
                    <td>{supplier_name}</td>
                    <td>{manufacture_date}</td>
                    <td>{oem}</td>
                </tr>
            )
        })
    }

    return (
        <>
            <h1 id='title'>React Table</h1>
            <table id='employee'>
                <thead>
                <tr>{renderHeader()}</tr>
                </thead>
                <tbody>
                {renderBody()}
                </tbody>
            </table>
        </>
    )
}

//REQUEST
//     {
//         "batch_id": "S202010100007",
//         "sku_number": "2222",
//         "sku_name": "tyre2222",
//         "manufacture_date": "10-10-20",
//         "oem_name": "oem"
//     }


//RESPONSE
// {
//     "status": "DONE",
//     "data": {}
// }

export default GetAllTransfersByASupplier;
