import React, {SyntheticEvent, useState} from 'react';
import {Redirect} from "react-router-dom";
import axios from "axios";

const Login = () => {
	const [name, setName] = useState('');
	const [password, setPassword] = useState('');
	const [redirect, setRedirect] = useState(false);


	const submit = async (e: SyntheticEvent) => {
		e.preventDefault();

		console.log({
			name,
			password
		})

		const data = {
			login: name,
			password: password
		}

		await axios.post('/auth/authorization', data)
			.then(res => {
				localStorage.setItem('token', res.data.data.token);
				//console.log("res.token is " + res.token);
				console.log("res is " + JSON.stringify(res));
				console.log("res.status is " + res.status);
				console.log("res.data.status is " + res.data.status);
				console.log("res.data.data.token is " + res.data.data.token);
			})
			.catch(err => {
				console.log(err);
			})

		setRedirect(true);
	}

//{
//     "login": "kent",
//     "password": "pass"
// }


// {
//     "status": "FAILED",
//     "error": "No account with that login exists"
// }

//{
//     "status": "DONE",
//     "data": {
//         "token": "eyJhbGciOiJIUzUxMiIsImlhdCI6MTYxNjQxODEwNSwiZXhwIjoxNjE2NTA0NTA1fQ.eyJsb2dpbiI6ImtlbnQiLCJwdWJsaWNfa2V5IjoiMDM2MTJiNmRiMjdhODk0OGQ5NjlhZTEwN2IzN2U1NjI3OGI0MDQ3YzE1MTlhODc4ZDE3NjUzMWMyYTc1NjI4NDM0In0.1HsDqv4Thrr_5DOjEWAYCk4v66khzbnyztxhHfPA5YGhTz35JpSnV5LT_DbjpdtgdanWgUtUWlFfe8P164jgBw"
//     }
// }

		if (redirect) {
			return <Redirect to="/menu" />;
		}

		return (
			<div>
				<form onSubmit={submit}>
					<h1 className="h3 mb-3 fw-normal">Please sign in</h1>
					<input type="username" className="form-control" placeholder="Username" required
						   onChange={e => setName(e.target.value)}
					/>

					<input type="password" className="form-control" placeholder="Password" required
						   onChange={e => setPassword(e.target.value)}
					/>

					<button className="w-100 btn btn-lg btn-primary" type="submit">Login</button>
				</form>
			</div>
		);
	}

export default Login;
