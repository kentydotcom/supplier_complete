import React from 'react';
import { Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Menu = () => {
    return (
        <Container>
            Make your selection:
            <br/>
            <br/>
            1) Provide Parts To OEM
            <br/>
            <br/>
            2) Get All Transfers By A Supplier
            <br/>
            <br/>

            <Link to="provide_parts">
            <Button variant="primary">
                Provide Parts
            </Button>
            </Link>

            <Link to="get_all_transfers_by_a_supplier">
                <Button variant="warning">
                    Get All
                </Button>
            </Link>
        </Container>
    );
};

export default Menu;
