import React, {SyntheticEvent, useState} from 'react';
import {Redirect} from "react-router-dom";
import axios from "axios";

const ProvideParts = () => {
    const [batch_id, setBatchId] = useState('');
    const [sku_number, setSkuNumber] = useState('');
    const [sku_name, setSkuName] = useState('');
    const [manufacture_date, setManufactureDate] = useState('');
    const [oem_name, setOemName] = useState('');
    const [redirect, setRedirect] = useState(false);

    const submit = async (e: SyntheticEvent) => {
        e.preventDefault();

        const data = {
            batch_id: batch_id,
            sku_number: sku_number,
            sku_name: sku_name,
            manufacture_date: manufacture_date,
            oem_name: oem_name
        }

        console.log(data)

        const tokenStr = localStorage.getItem('token')

        console.log(tokenStr);

        const config = {
            //headers: {"Authorization" : `Bearer ${tokenStr}`}
            headers: {"Authorization" : "supplier2"}
        };

        await axios.post(
            '/supplier/provide_parts',
            data,
            config
        )
            .then(res => {
                //console.log("res.token is " + res.token);
                console.log("res is " + JSON.stringify(res));
                console.log("res.status is " + res.status);
                console.log("res.data.status is " + res.data.status);
                console.log("res.data.data.token is " + res.data.data.token);
            })
            .catch(err => {
                console.log(err);
            })

        setRedirect(true);
    }

    if (redirect) {
        return <Redirect to="/transaction_success" />;
    }

    return (
        <div>
            <form onSubmit={submit}>
                <h1 className="h3 mb-3 fw-normal">
                    Provide Parts
                </h1>
                <input
                    type="batch_id"
                    className="form-control"
                    placeholder="Batch ID"
                    required
                    onChange={e => setBatchId(e.target.value)}
                />
                <input
                    type="sku_number"
                    className="form-control"
                    placeholder="SKU number #"
                    required
                    onChange={e => setSkuNumber(e.target.value)}
                />
                <input
                    type="sku_name"
                    className="form-control"
                    placeholder="SKU Name"
                    required
                    onChange={e => setSkuName(e.target.value)}
                />
                <input
                    type="manufacture_date"
                    className="form-control"
                    placeholder="Date Of Manufacture"
                    required
                    onChange={e => setManufactureDate(e.target.value)}
                />
                <input
                    type="oem_name"
                    className="form-control"
                    placeholder="Recipient OEM"
                    required
                    onChange={e => setOemName(e.target.value)}
                />

                <button
                    className="w-100 btn btn-lg btn-primary"
                    type="submit">
                    Submit
                </button>
            </form>
        </div>
    );
}
//REQUEST
//     {
//         "batch_id": "S202010100007",
//         "sku_number": "2222",
//         "sku_name": "tyre2222",
//         "manufacture_date": "10-10-20",
//         "oem_name": "oem"
//     }


//RESPONSE
// {
//     "status": "DONE",
//     "data": {}
// }

export default ProvideParts;
