import React, {SyntheticEvent, useState} from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

const Register = () => {
	const [name, setName] = useState('');
	const [password, setPassword] = useState('');
	const [redirect, setRedirect] = useState(false);

	const submit = async (e: SyntheticEvent) => {
		e.preventDefault();

		console.log({
			name,
			password
		})

		//{
		//     "name": "supplier",
		//     "password": "123"
		// }

		// const response =

		const data = {
			login: name,
			password: password
		}

		await axios.post('/supplier/new', data)
			.then(res => {
				localStorage.setItem('token', res.data.data.token);
				//console.log("res.token is " + res.token);
				console.log("res is " + JSON.stringify(res));
				console.log("res.status is " + res.status);
				console.log("res.data.status is " + res.data.status);
				console.log("res.data.data.token is " + res.data.data.token);
			})
			.catch(err => {
				console.log(err);
			})

/////////////////////////////////////////////////////////////////////////
//             await fetch(
//             '/supplier/new', {
//                 method: 'POST',
//                 mode: 'no-cors',
//                 headers: {'Content-Type': 'application/json'},
//                 body: JSON.stringify({
//                     login: name,
//                     password: password
//                 })
//             });
// //////////////////////////////////////////////////////////////////////

		// const data = await response.json();
		//
		// console.log(data);

		//{
		//     "status": "DONE",
		//     "data": {
		//         "token": "eyJhbGciOiJIUzUxMiIsImlhdCI6MTYxNjQxNjQ0MiwiZXhwIjoxNjE2NTAyODQyfQ.eyJsb2dpbiI6InN1cHBsaWVyMiIsInB1YmxpY19rZXkiOiIwMmYwMDcxZGFiNjE4ZjM0MTE5NWU1MDBlYTI0M2M5MTNjN2U3ZWM1YTlmMWZiMGQ2NzdiN2U0YWM1NjhkZTlhYzgifQ.8txpIY6VOvqs1jNib9e2o_d8esz1FAAyYex1oeHhV4f6sKxh5l5pTVwTOeBC1pxpQPHz9qA8A9PSoG6phI1M4g"
		//     }
		// }

		//{
		//     "status": "FAILED",
		//     "error": "login is required"
		// }

		setRedirect(true);
	}

	if(redirect) {
		return <Redirect to="/login"/>;
	}

	return (
			<form onSubmit={submit}>
				<h1 className="h3 mb-3 fw-normal">Please register</h1>

				<input type="username" className="form-control" placeholder="Username" required
					onChange={e => setName(e.target.value)}
				/>

				<input type="password" className="form-control" placeholder="Password" required
					   onChange={e => setPassword(e.target.value)}
				/>

				<button className="w-100 btn btn-lg btn-primary" type="submit">Submit</button>
			</form>
	);
}

export default Register;
