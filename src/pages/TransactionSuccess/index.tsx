import React from 'react';
import { Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProvideParts from "../ProvideParts";

const TransactionSuccess = () => {
    return (
        <Container>
            Transaction Success!

            <br/>
            <Link to="menu">
            <Button variant="success">
                Return
            </Button>
            </Link>

        </Container>
    );
};

export default TransactionSuccess;
